# PROYECTO PAREA

## ENTORNO DE TRABAJO

Para crear el entorno de trabajo y que sea fácilmente exportable a otros equipos se han adaptado los pasos vistos en esta guía: https://www.digitalocean.com/community/tutorials/containerizing-a-node-js-application-for-development-with-docker-compose-es

El primer paso sería clonar los repositorios de la API y del Front, que son submódulos Git:

```shell
git clone --recurse-submodules https://gitlab.com/angeldefez/parea.git
```

Si en algún momento quieres comprobar actualizaciones:

```shell
git submodule update --init --recursive
git submodule update --recursive --remote
git pull origin master --recurse-submodules
```

Si hubiera cambios, para subir los submódulos (push) a la vez a sus respectivos repositorios y luego hacer lo propio con el proyecto base podemos usar:

```shell
git push --recurse-submodules=on-demand
```

## INICAR LOS SERVICIOS

Se han creado contenedores Docker separados por cada servicio (la API REST, la BBDD MongoDB y el Cliente VUE).
Para que funcione, deberías crear el archivo `.env` con las variables de entorno básicas:

```shell
MONGO_USERNAME=user
MONGO_PASSWORD=password
MONGO_PORT=27017
MONGO_DB=database
TOKEN_SECRET=supersecret
```

> Nota: puedes cambiar todos los valores, menos el puerto de conexión a MongoDB.

Después, se crean las imágenes y se inician los servicios con:

```shell
docker-compose up -d
```

### Problemas conocidos

Si al clonar el repositorio únicamente se crean las carpetas de los submódulos (`parea-vue` y `parea-api`) se puede probar una combinación de comandos más clásica:

```shell
git clone https://gitlab.com/angeldefez/parea.git
cd parea
git submodule init
git submodule update
```

---

El archivo `whait-for.sh` debería tener permisos de ejecución (`sudo chmod +x whait-for.sh`) y tener una codificación de salto de línea de LF y no de CRLF. Esto pasa por no configurar bien el entorno Git, cosa que voy a intentar solucionar con la configuración del archivo `.gitattributes`.

Hay veces que al bajar las imágenes Docker da un error de build, para solucionar esto:

```shell
 docker-compose rm &&
 docker-compose pull &&
 docker-compose build --no-cache &&
 docker-compose up -d --force-recreate
```
